//
//  CalloutView.swift
//  WifiCaptive
//
//  Created by plotkin on 22/08/15.
//  Copyright © 2015 plotkin. All rights reserved.
//

import UIKit
//import HotspotHelper

class CalloutView: UIView, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var descField: UILabel!
    var desc:String!
    var view: UIView!
    var delegate: CalloutViewDelegate?
    
    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var buttonRoute: UIButton!
    
    @IBOutlet var viewItem: UIView!
    
    override init(frame: CGRect) {

        super.init(frame: frame)
        xibSetup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
        
    }
    
    func xibSetup() {
        
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        buttonRoute.titleLabel?.minimumScaleFactor = 0.5
        buttonRoute.titleLabel?.adjustsFontSizeToFitWidth = true
        
        addSubview(view)
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CalloutView.detailsTapped(_:))))
        
    }
    
    func loadViewFromNib() -> UIView {

        let nib = UINib(nibName: "CalloutView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @IBOutlet weak var directionsOutlet: UIButton!
    
    @IBAction func detailsTapped(_ sender: AnyObject) {
        delegate?.NavigateToHotspotDetails()
    }
    
}

protocol CalloutViewDelegate{
    func NavigateToHotspotDetails()
    func showPopUp()
    func showPopUpDirections()
}

