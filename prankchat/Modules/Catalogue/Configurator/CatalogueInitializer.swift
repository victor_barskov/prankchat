//
//  CatalogueInitializer.swift
//  prankchat
//
//  Created by Victor Barskov on 11/09/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public class CatalogueInitializer: NSObject, ViperModuleInitializer, ViperInitializer {
    
    @IBOutlet weak public var viewController: CatalogueViewController!
    
    public func createModule(values: [Any]) {
        
        let router = CatalogueRouter()
        let iapService = IAPService()
        let purchaseManager = PurchaseManager(IAPService: iapService)
        
        if viewController == nil {
            viewController = UIStoryboard(name: "Catalogue", bundle: nil).instantiateViewController(withIdentifier: "CatalogueViewController") as? CatalogueViewController
        }
        
        router.transitionHandler = viewController
        let presenter = CataloguePresenter(view: viewController, router: router, purchaseManager: purchaseManager)
        viewController.presenter = presenter
        
    }
}

