//
//  CatalogueViewOutput.swift
//  prankchat
//
//  Created by Victor Barskov on 11/09/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

protocol CatalogueViewOutput {
    func viewLoaded()
    func didSeletRow(type: ChatType)
    func didTapRestore()
}
