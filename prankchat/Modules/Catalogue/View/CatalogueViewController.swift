//
//  CatalogueViewController.swift
//  prankchat
//
//  Created by Victor Barskov on 11/09/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit

public class CatalogueViewController: UIViewController, ViperModuleTransitionHandler {
    
    // MARK: - Properties -
    var presenter: CatalogueViewOutput!
    let chatOptions: [ChatType] = [.messages, .whatsapp]
    
    // MARK: - Outlets -
    @IBOutlet weak var table: UITableView! {
        didSet {
            table.register(cellType: ChatOptionCell.self)
            table.separatorStyle = .none
        }
    }
    @IBOutlet weak var restoreButton: UIButton!
    
    @IBAction func didTapRestore(_ sender: UIButton) {
        presenter.didTapRestore()
    }
    
    
    // MARK: - Load -
    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
        
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView() {
        self.navigationItem.title = "Design options".localized(with_comment: "")
        restoreButton.setTitle("restore".localized(with_comment: ""), for: .normal)
    }
    
}

extension CatalogueViewController: UITableViewDelegate, UITableViewDataSource {
    
    // DataSource
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatOptions.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ChatOptionCell = tableView.dequeueReusableCell(for: indexPath)
        cell.setupWith(chatType: chatOptions[indexPath.row])
        return cell
        
    }
    
    // Delegate
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.didSeletRow(type: chatOptions[indexPath.row])
    }
    
}

extension CatalogueViewController: CatalogueViewInput {
    
}
