//
//  CatalogueViewInput.swift
//  prankchat
//
//  Created by Victor Barskov on 11/09/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public protocol CatalogueViewInput: class {
    // to place here a code that will notify presenter about view events
    func dismissKeyboard()
}

