//
//  ChatOptionCell.swift
//  Yourselfchat
//
//  Created by Victor Barskov on 11/09/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit
import Reusable

class ChatOptionCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var optionImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .default
    }
    
    func setupWith(chatType: ChatType) {
        switch chatType {
        case .messages:
            optionImage.image = UIImage(named: "messagesIconBlue")
            break
        case .whatsapp:
            optionImage.image = UIImage(named: "whatsAppLabel")
            break
        }
    }
}
