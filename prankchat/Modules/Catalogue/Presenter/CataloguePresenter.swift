//
//  CataloguePresenter.swift
//  prankchat
//
//  Created by Victor Barskov on 11/09/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
import StoreKit
import KeychainAccess

class CataloguePresenter {
    
    private var alertShowed = false
    private weak var view: CatalogueViewInput!
    private let router: CatalogueRouterInput!
    var purchaseManager: IPurchaseManager
    
    init(view: CatalogueViewInput, router: CatalogueRouterInput, purchaseManager: IPurchaseManager) {
        self.view = view
        self.router = router
        self.purchaseManager = purchaseManager
        
        // Subscribe to a notification that fires when a product is purchased.
        NotificationCenter.default.addObserver(self, selector: #selector(self.productPurchased(_:)), name: NSNotification.Name(rawValue: IAPHelperProductPurchasedNotification), object: nil)
        // Subscribe to a notification that fires when the purchase is failed
        NotificationCenter.default.addObserver(self, selector: #selector(self.transactionFailed(_:)), name: NSNotification.Name(rawValue: IAPHelperProductTransactionError), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.productRestored(_:)), name: NSNotification.Name(rawValue: IAPHelperProductPurchasRestoreNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func transactionFailed (_ notification: Notification) {
        progressHUD(false)
        if !alertShowed {
            systemAlert(title: "Sorry..".localized(with_comment: ""), message: "Transaction is cancelled! Please try again later".localized(with_comment: ""))
            alertShowed = true
        }
    }
    
    @objc func productPurchased(_ notification: Notification) {
        progressHUD(false)
        prankChatKeychain["WhatsAppCharged"] = charged
        GA.event(whatsAppProductPurchased)
        showWhatsAppModule()
    }
    
    @objc func productRestored(_ notification: Notification) {
        progressHUD(false)
        GA.event(whatsAppProductRestored)
        prankChatKeychain["WhatsAppCharged"] = charged
        systemAlert(title: "Success".localized(with_comment: ""), message: "Restored successfully!".localized(with_comment: ""))
    }
    
    private func buyProduct(_ product: SKProduct) {
        progressHUD(true)
        purchaseManager.purchaseProduct(product)
    }
    
    private func saveDefaultChatType(_ type: ChatType) {
        UserDefaults.standard.set(type.rawValue, forKey: defaultChatType)
        UserDefaults.standard.synchronize()
    }
    
}

extension CataloguePresenter: CatalogueViewOutput {
    
    func didSeletRow(type: ChatType) {
        switch type {
        case .messages:
            router.openChat(chatType: .messages)
            break
        case .whatsapp:
            showWhatsApp()
            break
        }
    }
    
    func viewLoaded() {}
    
    private func showWhatsApp() {
        
        #if DEBUG
        showWhatsAppModule()
        #else
        if prankChatKeychain["WhatsAppCharged"] != charged {
             progressHUD(true)
            purchaseManager.requestProducts([whatsAppModuleAccessIdentifier]) {[weak self] (products) in
                if products == nil {
                    systemAlert(title: "Sorry..".localized(with_comment: ""), message: "Something went wrong...Please try again later".localized(with_comment: ""))
                    return
                }
                guard let onlyProduct = products?.first else {
                    return
                }
                self?.buyProduct(onlyProduct)
            }
        } else {
            showWhatsAppModule()
        }
        #endif
    }
    
    private func showWhatsAppModule() {
        progressHUD(false)
        router.openChat(chatType: .whatsapp)
    }
    
    func didTapRestore() {
        purchaseManager.restore()
    }
}
