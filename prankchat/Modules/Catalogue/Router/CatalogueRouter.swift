//
//  CatalogueRouter.swift
//  Yourselfchat
//
//  Created by Victor Barskov on 11/09/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public protocol CatalogueRouterInput {
    func moduleIsReady()
    func openChat(chatType: ChatType)
}

public class CatalogueRouter {
    
    weak var transitionHandler: ViperModuleTransitionHandler!
    
    func openModule(with transitionHandler: ViperModuleTransitionHandler, transitionStyle: TransitionStyle) {
        let initializer = CatalogueInitializer.assemble()
        transitionHandler.openModule(vc: initializer.viewController, style: transitionStyle)
    }
    
    func createModule() -> UIViewController? {
        let initializer = CatalogueInitializer.assemble()
        return initializer.viewController
    }
}

extension CatalogueRouter: CatalogueRouterInput {
    public func openChat(chatType: ChatType) {
        switch chatType {
        case .messages:
            if let transitionHandler = transitionHandler {
                MessagesRouter().openModule(with: transitionHandler, transitionStyle: .push)
            }
            break
        case .whatsapp:
            if let transitionHandler = transitionHandler {
                WhatsAppRouter().openModule(with: transitionHandler, transitionStyle: .push)
            }
            break
        }
    }    
    public func moduleIsReady() {
        // do something
    }
    
    
}

