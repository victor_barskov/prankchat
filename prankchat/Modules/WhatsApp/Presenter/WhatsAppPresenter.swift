//
//  WhatsAppPresenter.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public class WhatsAppPresenter: ViperModuleOutputProtocol {
    
    var messages: [Date: [Question]] = [:]
    var sections: [Date] = []
    
    weak public var view: IWhatsAppViewInput!
    public var router: IWhatsAppRouterInput!
    
    init(view: IWhatsAppViewInput, router: WhatsAppRouter) {
        self.view = view
        self.router = router
    }
    
}

// MARK: - MessagesViewOutput

extension WhatsAppPresenter: WhatsAppViewOutput {
    
    
    func shouldShowSendButton(_ show: Bool) {
        view.showSendButton(isShown: show)
    }
    
    func didTapShowAvatar(_ show: Bool) {
        view.showAvatar(show)
        view.showHideSettings(false)
    }
    
    func didTapDoneInKeybordReturn(_ text: String?) {
        view.setAvatarObjects(text, image: nil, avatarLabel: nil, backColor: nil)
        view.dismissKeyboard()
    }

    func didTapSend(message: String?, isFromUser: Bool, messageType: MessageType) {
        
        guard let message = message else {return}
        if message.isEmptyField {
            view.dismissKeyboard()
            return
        }
        let q = Question(message: message, time: Date().formatToString("HH:mm", true), isFromUser: isFromUser, messageType: messageType)
        let key = Date().startOfDay
        if messages[key] != nil {
            messages[key]!.append(q)
        } else {
            messages[key] = [q]
            sections.append(key)
        }
        view.setData(messages: messages, sections: sections)
        
        //        for s in sections {
        //            if let messages = messages[s] {
        //                for m in messages {
        //                    print("Message time: \(m.time) value: \(m.message)")
        //                }
        //            }
        //        }
        view.dismissKeyboard()
        
    }
    
    func didTapClear() {
        if self.messages.count > 0, self.sections.count > 0 {
            self.messages.removeAll()
            self.sections.removeAll()
            view.setData(messages: messages, sections: sections)
        }
        view.clearAvatar()
        view.showHideSettings(false)
    }
    
    func didTapShowSettings() {
        router.showSettingsView()
    }
    
    func didTapChangeAvatar() {
        view.setAvatarPicture()
    }
    
    func didSwipe(_ direction: UISwipeGestureRecognizerDirection) {
        switch direction {
        case .up:
            view.showHideSettings(false)
            break
        case .down:
            view.showHideSettings(true)
            break
        case .right:
            view.addRemoveInputUserTypeView(true)
            break
        case .left:
            view.addRemoveInputUserTypeView(false)
            break
        default:
            break
        }
    }
    
    
    func viewLoaded() {
        
    }
    
    func viewAppeared() {
//        view.setChatBackgroundImage(imageString: "whatsAppStandardWalpaper")
    }
    
    func saveDefaultChatType(_ type: ChatType) {
        UserDefaults.standard.set(type.rawValue, forKey: defaultChatType)
        UserDefaults.standard.synchronize()
    }
    
    func didTapBack() {
        view.back()
    }
    
    func didTapVideo() {
        systemAlert(title: "Sorry..".localized(with_comment: ""), message: "This function will be available soon".localized(with_comment: ""))
    }
    
    func didTapPhone() {
        systemAlert(title: "Sorry..".localized(with_comment: ""), message: "This function will be available soon".localized(with_comment: ""))
    }
    
    func didTapPlus() {
        systemAlert(title: "Sorry..".localized(with_comment: ""), message: "This function will be available soon".localized(with_comment: ""))
    }
    
    func didTapPhoto() {
        systemAlert(title: "Sorry..".localized(with_comment: ""), message: "This function will be available soon".localized(with_comment: ""))
    }
    
    func didTapVoice() {
        systemAlert(title: "Sorry..".localized(with_comment: ""), message: "This function will be available soon".localized(with_comment: ""))
    }
    
}




