//
//  WhatsAppAvatarBackgroundView.swift
//  prankchat
//
//  Created by Victor Barskov on 21/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

class WhatsAppAvatarView: UIView {
    private var gradientLayer: RadialGradientLayer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        backgroundColor = UIColor(hex: "969BA7", alpha: 1.0)
        gradientLayer = RadialGradientLayer()
        gradientLayer?.colors = [
            UIColor(hex: "969BA7", alpha: 1.0), UIColor.lightGray
        ]
        guard let gradientLayer = gradientLayer else {
            return
        }
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradientLayer?.frame = bounds
    }
}
