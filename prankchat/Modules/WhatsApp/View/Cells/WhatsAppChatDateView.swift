//
//  ChatDateView.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit

class WhatsAppChatDateView: UITableViewHeaderFooterView {

    @IBOutlet weak var dateLabel: UILabel!
    
    func setupWith(str: String) {
        dateLabel.text = str
    }

}
