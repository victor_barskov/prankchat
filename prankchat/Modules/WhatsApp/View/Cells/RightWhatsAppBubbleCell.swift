//
//  RightWhatsAppTableViewCell.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit
import Reusable

class RightWhatsAppBubbleCell: UITableViewCell, NibReusable {

    @IBOutlet weak var messageLabel: WhiteTitleLabel!
    @IBOutlet weak var timestamp: UILabel!
    @IBOutlet weak var bubbleImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
    func setupWith(message: String, timeStamp: String) {
        messageLabel.text = message
        timestamp.text = timeStamp
        messageLabel.textColor = .black
        bubbleImage.image = UIImage(named: "green_sent")?.resizableImage(withCapInsets:UIEdgeInsetsMake(17, 21, 17, 21),resizingMode: .tile).withRenderingMode(.alwaysOriginal)
    }

}
