//
//  WhatsAppViewController.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit
import Reusable

public class WhatsAppViewController: UIViewController, ViperModuleTransitionHandler {

    // MARK: - Properties -
    var presenter: WhatsAppViewOutput!
    lazy var dataSource: WhatsAppTableViewDataSource = WhatsAppTableDataSource()
    var backroundImagePicker: UIImagePickerController!
    var avatarImagePicker : UIImagePickerController!
    var isFromUserView: UIView!
    var isFromUser = true
    var settingsViewShowed = false
    var avatarIsShown = true
    var sendButtonIsShown = false
    
    var messages: [Date: [Question]] = [:]
    var sections: [Date] = []
    
    // MARK: - Outlets -
    @IBOutlet weak var whatsAppChatBackground: UIImageView! {
        didSet{
        }
    }
    
    // Info
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var noMessagesInfoLabel: UILabel!
    @IBOutlet weak var infoView: UIView!
    
    // SettingsView
    
    @IBOutlet weak var setAvatarButton: UIButton!

    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var settingsViewTopConstraint: NSLayoutConstraint!
    @IBAction func clearAction(_ sender: UIButton) {presenter.didTapClear()}

    
    @IBAction func didTapShowAvatar(_ sender: UIButton) {
        if avatarIsShown {
            setAvatarButton.setImage(UIImage(named: "defaultAvatar"), for: .normal)
            presenter.didTapShowAvatar(false)
        } else {
            setAvatarButton.setImage(UIImage(named: "defaultAvatarNo"), for: .normal)
            presenter.didTapShowAvatar(true)
        }
    }
    
    // Header
    @IBOutlet weak var avatarButton: UIButton!
    @IBOutlet weak var avatarView: WhatsAppAvatarView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var avatarLabel: UILabel!
    @IBAction func avatarButtonAction(_ sender: UIButton) {presenter.didTapChangeAvatar()}
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var noAvatarLabelConstraint: NSLayoutConstraint!
    @IBAction func backAction(_ sender: UIButton) {presenter.didTapBack()}
    @IBAction func didTapVideo(_ sender: UIButton) {presenter.didTapVideo()}
    @IBAction func didTapPhone(_ sender: UIButton) {presenter.didTapPhone()}
    
    // Table
    @IBOutlet weak var table: UITableView! {
        didSet {
            let nibName = UINib(nibName: "WhatsAppChatDateView", bundle: nil)
            table.register(nibName, forHeaderFooterViewReuseIdentifier: "WhatsAppChatDateView")
            table.separatorStyle = .none
            table.register(cellType: RightWhatsAppBubbleCell.self)
            table.register(cellType: LeftWhatsAppBubbleCell.self)
            dataSource.delegate = self
            dataSource.messages = [:]
            table.dataSource = dataSource
            table.delegate = dataSource
        }
    }
    
    @IBAction func sendButtonAction(_ sender: UIButton) {
        
        if !sendButtonIsShown {
            presenter.didTapVoice()
        } else {
            presenter.didTapSend(message: inputTextField?.text, isFromUser: isFromUser, messageType: .whatsApp)
            presenter.shouldShowSendButton(false)
            sendButtonIsShown = false
            inputTextField?.text = ""
        }
        
    }
    
    // InputTextField
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var whatsAppImageInputButton: UIButton!
    @IBOutlet weak var inputTextContentView: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var inputTextContentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var constraintToSendButtonInputFooterView: NSLayoutConstraint!
    @IBAction func textDidChange(_ sender: UITextField) {
        guard let charactersCount = sender.text?.count else {return}
        if charactersCount > 0 {
            presenter.shouldShowSendButton(true)
        } else {
            presenter.shouldShowSendButton(false)
        }
    }
    
    @IBAction func didTapPlus(_ sender: UIButton) {presenter.didTapPlus()}
    @IBAction func didTapPhoto(_ sender: UIButton) {presenter.didTapPhoto()}

    
    // MARK: - Load -
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        presenter.viewLoaded()
        setupView()
        addTapGestureWithCallback()
        addGestureRecognizers()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        presenter.viewAppeared()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Set up -
    
    public func setAvatarObjects(_ name: String?, image: UIImage?, avatarLabel: String?, backColor: UIColor?) {
        if let name = name {
            nameTextField.text = name
        }
        if let image = image {
            avatarImageView.image = image
        }
        if let avatarLabel = avatarLabel {
            self.avatarLabel.text = avatarLabel
        }
        if let backColor = backColor {
            avatarView.backgroundColor = backColor
        }
    }
    
    func setupView() {
        
        // Info
//        noMessagesInfoLabel.text = "Prankchat".localized(with_comment: "")
        // Input
        inputTextField.layer.cornerRadius = 16.0
        inputTextField.layer.borderWidth = 0.8
        inputTextField.layer.borderColor = UIColor(hex: "C1C1C7", alpha: 0.8).cgColor
        inputTextField.layer.masksToBounds = true
        inputTextField.placeholder = ""
        
        // Header
        avatarView.layer.cornerRadius = avatarView.frame.size.width / 2
        avatarView.clipsToBounds = true
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
        avatarImageView.clipsToBounds = true
        nameTextField.placeholder = "Set name".localized(with_comment: "")
        nameTextField.font = UIFont.boldSystemFont(ofSize: 16)
        
    }
    
    // MARK: - Helpers -
    
    public func setChatBackgroundImage(imageString: String) {
         whatsAppChatBackground.image = UIImage(named: imageString)
    }
    public func back() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func keyboardNotification(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve: UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if view.selectedTextField?.tag == 1 {
            } else if view.selectedTextField?.tag == 2 {
                if endFrameY >= UIScreen.main.bounds.size.height {
                    self.inputTextContentViewBottomConstraint?.constant = 0.0
                } else {
                    let window = UIApplication.shared.keyWindow
                    if #available(iOS 11.0, *) {
                        if let bottomPadding = window?.safeAreaInsets.bottom {
                            self.inputTextContentViewBottomConstraint?.constant = (endFrame?.size.height ?? 0.0) - bottomPadding
                        } else {
                            self.inputTextContentViewBottomConstraint?.constant = (endFrame?.size.height ?? 0.0)
                        }
                    } else {
                        // Fallback on earlier versions
                         self.inputTextContentViewBottomConstraint?.constant = (endFrame?.size.height ?? 0.0)
                    }
                }
            }
            if self.settingsViewShowed {
                self.showHideSettings(false)
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: {_ in
                           self.updateTable()
                           self.view.layoutSubviews()
            })
        }
    }
    
    func hideTable(_ hide: Bool) {
        table.isHidden = hide
    }
    
    public func showHideSettings(_ show: Bool) {
        if show {
            if !settingsViewShowed {
                self.settingsViewShowed = true
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.beginFromCurrentState], animations: {
                    self.settingsView.frame = CGRect(x: 0, y:self.header.frame.origin.y + self.header.frame.size.height, width: self.view.frame.size.width, height: self.settingsView.frame.size.height)
                }, completion: {_ in
                })
            }
        } else {
            self.settingsViewShowed = false
            UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.beginFromCurrentState], animations: {
                self.settingsView.frame = CGRect(x: 0, y: self.header.frame.origin.y, width: self.view.frame.size.width, height: self.settingsView.frame.size.height)
            }, completion: {_ in
            })
        }
    }
    
    public func clearAvatar() {
        self.avatarLabel.text = "+"
        self.avatarImageView?.image = nil
        self.avatarLabel.isHidden = false
        self.nameTextField.text = ""
    }
    
    @objc public func didTapSetWhatsAppChatBackground() {
        self.setWhatsAppChatBackroundPicture()
        self.removeView(with: setBackgroundViewTag)
    }
    
}

extension WhatsAppViewController {
    
    public func addRemoveInputUserTypeView (_ isFromUser: Bool) {
        showHideSettings(false)
        if isFromUser {
            self.isFromUserView?.removeFromSuperview()
            let isFromUserViewRect = CGRect(x: self.view.frame.size.width / 2, y: header.frame.origin.y + header.frame.size.height, width: self.view.frame.size.width / 2, height: whatsAppIsFromUserHeightConstantRight)
            isFromUserView = UIView(frame: isFromUserViewRect)
            isFromUserView.backgroundColor = .whatsAppBrandColor
            UIView.animate(withDuration: 0.2) {
                self.view.addSubview(self.isFromUserView)
            }
            delay(2) {
                UIView.animate(withDuration: 0.2, animations: {
                    self.isFromUserView.alpha = 0.0
                }, completion: { (completed) in
                })
            }
            self.isFromUser = true
        } else {
            self.isFromUserView?.removeFromSuperview()
            let isFromUserViewRect = CGRect(x: 0, y: header.frame.origin.y + header.frame.size.height, width: self.view.frame.size.width / 2, height: whatsAppIsFromUserHeightConstantLeft)
            isFromUserView = UIView(frame: isFromUserViewRect)
            isFromUserView.backgroundColor = .lightGray
            UIView.animate(withDuration: 0.2) {
                self.view.addSubview(self.isFromUserView)
            }
            delay(2) {
                UIView.animate(withDuration: 0.2, animations: {
                    self.isFromUserView.alpha = 0.0
                }, completion: { (completed) in
                })
            }
            self.isFromUser = false
        }
    }
}

extension WhatsAppViewController {
    
    func addGestureRecognizers () {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
        self.view.addGestureRecognizer(longPressRecognizer)
        
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            presenter.didSwipe(gesture.direction)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            presenter.didSwipe(gesture.direction)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            presenter.didSwipe(gesture.direction)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            presenter.didSwipe(gesture.direction)
        }
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        
        let point = sender.location(in: self.view)
        #if DEBUG
        print("\(point.x) \(point.y)")
        #endif
        
        // Check if point of touch is in the tableView/infoScreen frame
        
        // Show action view depends on where it was tapped:
        // - tapped in tableView/infoScreen frame - set backround action
        // - tapped on cell - actions for cells (edit/delete)
        
        self.removeView(with: setBackgroundViewTag)
        let setBackgroundView = SetBackgroundView.loadNib()
        setBackgroundView.frame = CGRect(x: point.x - 60, y: point.y - 60, width: 120, height: 30)
        setBackgroundView.tag = setBackgroundViewTag
        setBackgroundView.setButton.addTarget(self, action: #selector(self.didTapSetWhatsAppChatBackground), for: .touchUpInside)
        self.view.addSubview(setBackgroundView)
        
    }
}

// MARK: Extension Delegates

extension WhatsAppViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate  {
    
    public func setWhatsAppChatBackroundPicture() {
        backroundImagePicker = UIImagePickerController()
        backroundImagePicker.delegate = self
        backroundImagePicker.sourceType = .photoLibrary;
        self.present(backroundImagePicker, animated: true, completion: nil)
    }
    
    public func setAvatarPicture() {
        avatarImagePicker = UIImagePickerController()
        avatarImagePicker.delegate = self
        avatarImagePicker.sourceType = .photoLibrary
        self.present(avatarImagePicker, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {return}
        
        if picker == avatarImagePicker {
            self.dismiss(animated: false, completion: { () -> Void in
                var imageCropVC : RSKImageCropViewController!
                imageCropVC = RSKImageCropViewController(image: image, cropMode: RSKImageCropMode.circle)
                imageCropVC.delegate = self
                self.present(imageCropVC, animated: true, completion: nil)
            })
        } else {
            self.dismiss(animated: true) {
                self.whatsAppChatBackground?.image = image
            }
        }
    }
    
    public func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        self.dismiss(animated: true) {
            self.avatarImageView?.image = croppedImage
            self.avatarLabel.isHidden = true
        }
    }
}

extension WhatsAppViewController: IWhatsAppViewInput {
    
    public func showSendButton(isShown: Bool) {
        sendButtonIsShown = isShown
        if isShown {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.beginFromCurrentState], animations: {
                self.whatsAppImageInputButton.isHidden = isShown
                self.sendButton.isUserInteractionEnabled = isShown
                self.sendButton.setImage(UIImage(named: "whatsAppSendButton"), for: .normal)
                self.constraintToSendButtonInputFooterView.constant = 8
                self.view.layoutIfNeeded()
            }, completion: {_ in
            })
        } else {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.beginFromCurrentState], animations: {
                
                self.sendButton.setImage(UIImage(named: "whatsAppMicrophone"), for: .normal)
                self.whatsAppImageInputButton.isHidden = isShown
                self.sendButton.isUserInteractionEnabled = !isShown
                self.constraintToSendButtonInputFooterView.constant = 46
                self.view.layoutIfNeeded()
                
            }, completion: {_ in
            })
        }
    }
    
    public func showAvatar(_ isShown: Bool) {
        if isShown {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.beginFromCurrentState], animations: {
                self.noAvatarLabelConstraint.constant = 64
                self.avatarView.isHidden = !isShown
                self.avatarImageView.isHidden = !isShown
                self.avatarView.isHidden = !isShown
                self.avatarButton.isHidden = !isShown
                self.view.layoutIfNeeded()
            }, completion: {_ in
                
            })
            
        } else {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.beginFromCurrentState], animations: {
                self.avatarView.isHidden = !isShown
                self.avatarImageView.isHidden = !isShown
                self.noAvatarLabelConstraint.constant = 8
                self.avatarView.isHidden = !isShown
                self.avatarButton.isHidden = !isShown
                self.view.layoutIfNeeded()
            }, completion: {_ in
            })
            
        }
        self.avatarIsShown = isShown
    }
    
    public func setData(messages: [Date : [Question]], sections: [Date]) {
        if messages.count > 0 {
            infoView.isHidden = true
            dataSource.messages = messages
            dataSource.sections = sections
            updateTable()
        } else {
            infoView.isHidden = false
        }
    }
    
    public func settingsViewShow(show: Bool) {
        
    }
}

extension WhatsAppViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text != nil {
            if view.selectedTextField?.tag == 1 {
                presenter.didTapDoneInKeybordReturn(view.selectedTextField?.text)
            } else if view.selectedTextField?.tag == 2 {
                presenter.didTapSend(message: view.selectedTextField?.text, isFromUser: isFromUser, messageType: .whatsApp)
                presenter.shouldShowSendButton(false)
                sendButtonIsShown = false
                textField.text = ""
            }
            return false
        }
        return true
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if view.selectedTextField?.tag == 1 {
            self.inputTextContentViewBottomConstraint?.constant = 0.0
        }
    }
}

extension WhatsAppViewController: WhatsAppTableViewDelegate {
    public func updateTable() {
        table.reloadData()
        table.scrollToBottom(animated: true)
    }
}

extension WhatsAppViewController {
    
    func addTapGestureWithCallback() {
        self.view.addTapGestureRecognizer {
            if self.settingsViewShowed {
                self.showHideSettings(false)
            }
            self.dismissKeyboard()
            self.removeView(with: setBackgroundViewTag)
        }
    }
    
    func hideSettingsWhenTapped() {
        let tapOnScreen: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideSettingsView))
        self.view.addGestureRecognizer(tapOnScreen)
    }
    @objc func hideSettingsView() {
        if settingsViewShowed {
            self.showHideSettings(false)
        }
    }
}
