//
//  RightMessageBubbleView.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit

class RightWhatsAppBubbleView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        self.layer.cornerRadius = 8
//        self.clipsToBounds = true
        self.backgroundColor = UIColor.clear
        self.addShadow(with: UIColor.coolGray.cgColor)
    }
}
