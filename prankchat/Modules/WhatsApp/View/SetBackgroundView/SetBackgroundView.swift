//
//  SetBeckgroundView.swift
//  prankchat
//
//  Created by Victor Barskov on 28/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit

class SetBackgroundView: UIView {
    @IBOutlet weak var viewItem: NSObject!
    @IBOutlet weak var back: UIView!
    @IBOutlet weak var setButton: UIButton!
}
