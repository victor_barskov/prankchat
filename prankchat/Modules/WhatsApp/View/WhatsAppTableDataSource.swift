//
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit
import Reusable

public protocol WhatsAppTableViewDataSource: UITableViewDataSource, UITableViewDelegate {
    var delegate: WhatsAppTableViewDelegate? { get set }
    var messages: [Date: [Question]] { get set }
    var sections: [Date] { get set }
}

public protocol WhatsAppTableViewDelegate: class {
    func updateTable()
}

public class WhatsAppTableDataSource: NSObject, WhatsAppTableViewDataSource {
    
    public var messages: [Date: [Question]] = [:]
    public var sections: [Date] = []
    public weak var delegate: WhatsAppTableViewDelegate?
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let a = messages[sections[section]] {
            return a.count
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "WhatsAppChatDateView") as? WhatsAppChatDateView {
            var dateTitle: String
            if sections[section].year == Date().year {
                dateTitle = DateFormatter.dateFormatter.string(from: sections[section])
            } else {
                dateTitle = DateFormatter.dateWithYearFormater.string(from: sections[section])
            }
            if Calendar.current.isDateInToday(sections[section]) {
                dateTitle = "Today".localized(with_comment: "")
            }
            header.setupWith(str: dateTitle)
            return header
        }
        return UIView()
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let array = messages[sections[indexPath.section]] {
            let object = array[indexPath.row]
            if object.isFromUser {
                let cell: RightWhatsAppBubbleCell = tableView.dequeueReusableCell(for: indexPath)
                cell.setupWith(message: object.message, timeStamp: object.time)
                return cell
            } else {
                let cell: LeftWhatsAppBubbleCell = tableView.dequeueReusableCell(for: indexPath)
                cell.setupWith(message: object.message, timeStamp: object.time)
                return cell
            }
        }
        return UITableViewCell()
    }
    
}

