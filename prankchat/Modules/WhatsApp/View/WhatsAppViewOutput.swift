//
//  WhatsAppViewOutput.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

protocol WhatsAppViewOutput {
    func viewLoaded()
    func viewAppeared()
    func saveDefaultChatType(_ type: ChatType)
    func didTapDoneInKeybordReturn(_ text: String?)
    func didTapSend(message: String?, isFromUser: Bool, messageType: MessageType)
    func didTapClear()
    func didTapShowAvatar(_ show: Bool)
    func shouldShowSendButton(_ show: Bool)
    func didTapShowSettings()
    func didTapChangeAvatar()
    func didSwipe(_ direction: UISwipeGestureRecognizerDirection)
    func didTapBack()
    func didTapVideo()
    func didTapPhone()
    func didTapPlus()
    func didTapPhoto()
    func didTapVoice()
    
}
