//
//  WhatsAppInitializer.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
import Reusable

public class WhatsAppInitializer: NSObject, ViperModuleInitializer, ViperInitializer {
    
    @IBOutlet weak public var viewController: WhatsAppViewController!
    
    public func createModule(values: [Any]) {
        
        let router = WhatsAppRouter()
        
        if viewController == nil {
            viewController = UIStoryboard(name: "WhatsApp", bundle: nil).instantiateViewController(withIdentifier: "WhatsAppViewController") as! WhatsAppViewController
        }
        
        router.transitionHandler = viewController
        
        let presenter = WhatsAppPresenter(view: viewController, router: router)
        
        viewController.presenter = presenter
        
    }
}
