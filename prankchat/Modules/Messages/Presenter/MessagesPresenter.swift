//
//  MessagesPresenter.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
import StoreKit
import KeychainAccess


public class MessagesPresenter: ViperModuleOutputProtocol {
    
    var alertShowed = false
    
    var messages: [Date: [Question]] = [:]
    var sections: [Date] = []
    var purchaseManager: IPurchaseManager
    
    weak public var view: IMessagesViewInput!
    public var router: IMessagesRouterInput!
    
    init(view: IMessagesViewInput, router: MessagesRouter, purchaseManager: IPurchaseManager) {
        self.view = view
        self.router = router
        self.purchaseManager = purchaseManager
        
        // Subscribe to a notification that fires when a product is purchased.
        NotificationCenter.default.addObserver(self, selector: #selector(self.productPurchased(_:)), name: NSNotification.Name(rawValue: IAPHelperProductPurchasedNotification), object: nil)
        // Subscribe to a notification that fires when the purchase is failed
        NotificationCenter.default.addObserver(self, selector: #selector(self.transactionFailed(_:)), name: NSNotification.Name(rawValue: IAPHelperProductTransactionError), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.productRestored(_:)), name: NSNotification.Name(rawValue: IAPHelperProductPurchasRestoreNotification), object: nil)
        
    }
    
    deinit {
         NotificationCenter.default.removeObserver(self)
    }
    
    @objc func transactionFailed (_ notification: Notification) {
        progressHUD(false)
        if !alertShowed {
            systemAlert(title: "Sorry..".localized(with_comment: ""), message: "Transaction is cancelled! Please try again later".localized(with_comment: ""))
            alertShowed = true
        }
    }
    
    @objc func productPurchased(_ notification: Notification) {
        progressHUD(false)
        prankChatKeychain["WhatsAppCharged"] = charged
        GA.event(whatsAppProductPurchased)
        showWhatsAppModule()
    }
    
    @objc func productRestored(_ notification: Notification) {
        progressHUD(false)
        GA.event(whatsAppProductRestored)
        prankChatKeychain["WhatsAppCharged"] = charged
            systemAlert(title: "Success".localized(with_comment: ""), message: "Restored successfully!".localized(with_comment: ""))
    }
    
}

// MARK: - MessagesViewOutput

extension MessagesPresenter: MessagesViewOutput {
    
    
    func didTapDoneInKeybordReturn(_ text: String?) {
        guard let firstLetter = text?.first else {return}
        let bigFirstLetter = String(firstLetter).uppercased()
        view.setAvatarObjects(text, image: nil, avatarLabel: bigFirstLetter, backColor: nil)
        view.dismissKeyboard()
    }

    
    func didTapSend(message: String?, isFromUser: Bool, messageType: MessageType) {
        
        guard let message = message else {return}
        if message.isEmptyField {
            view.dismissKeyboard()
            return
        }
        let q = Question(message: message, time: Date().formatToString("hh:mm", true), isFromUser: isFromUser, messageType: messageType)
        let key = Date().startOfDay
        if messages[key] != nil {
            messages[key]!.append(q)
        } else {
            messages[key] = [q]
            sections.append(key)
        }
        view.setData(messages: messages, sections: sections)
        
        //        for s in sections {
        //            if let messages = messages[s] {
        //                for m in messages {
        //                    print("Message time: \(m.time) value: \(m.message)")
        //                }
        //            }
        //        }
        
        view.dismissKeyboard()
        
    }
    
    func didTapClear() {
        if self.messages.count > 0, self.sections.count > 0 {
            self.messages.removeAll()
            self.sections.removeAll()
            view.setData(messages: messages, sections: sections)
        }
        view.clearAvatar()
        view.showHideSettings(false)
    }
    
    func didTapShowSettings() {
        router.showSettingsView()
    }
    
    func didTapChangeAvatar() {
        view.setAvatarPicture()
    }
    
    func didSwipe(_ direction: UISwipeGestureRecognizerDirection) {
        switch direction {
        case .up:
            view.showHideSettings(false)
            break
        case .down:
            view.showHideSettings(true)
            break
        case .right:
            view.addRemoveInputUserTypeView(true)
            break
        case .left:
            view.addRemoveInputUserTypeView(false)
            break
        default:
            break
        }
    }
    
    
    func viewLoaded() {
    }
    
    func viewAppeared() {
    }
    
    func saveDefaultChatType(_ type: ChatType) {
        UserDefaults.standard.set(type.rawValue, forKey: defaultChatType)
        UserDefaults.standard.synchronize()
    }
    
    func didTapShowWhatsApp() {
        
        progressHUD(true)
        view.showHideSettings(false)
        #if DEBUG
        showWhatsAppModule()
        #else
        if prankChatKeychain["WhatsAppCharged"] != charged {
            purchaseManager.requestProducts([whatsAppModuleAccessIdentifier]) {[weak self] (products) in
                if products == nil {
                    systemAlert(title: "Sorry..".localized(with_comment: ""), message: "Something went wrong...Please try again later".localized(with_comment: ""))
                    return
                }
                guard let onlyProduct = products?.first else {
                    return
                }
                self?.buyProduct(onlyProduct)
            }
        } else {
            showWhatsAppModule()
        }
        #endif

    }
    
    func buyProduct(_ product: SKProduct) {
        progressHUD(true)
        purchaseManager.purchaseProduct(product)
    }
    
    func showWhatsAppModule() {
        if let window = (UIApplication.shared.delegate as? AppDelegate)?.window {
            window.makeKeyAndVisible()
            if let vc = WhatsAppRouter().createModule() {
                self.saveDefaultChatType(.whatsapp)
                window.rootViewController = UINavigationController(rootViewController: vc)
            }
        }
    }
    
    func didTapRestore() {
        purchaseManager.restore()
    }
    
    func didTapBack() {
        view.back()
    }
    
    func didTapInfo() {
        view.showHideSettings(true)
    }
    
    func didTapPhoto() {
        systemAlert(title: "Sorry..".localized(with_comment: ""), message: "This function will be available soon".localized(with_comment: ""))
    }
    func didTapApps() {
        systemAlert(title: "Sorry..".localized(with_comment: ""), message: "This function will be available soon".localized(with_comment: ""))
    }
    func didTapVoice() {
        systemAlert(title: "Sorry..".localized(with_comment: ""), message: "This function will be available soon".localized(with_comment: ""))
    }
        
}




