//
//  IMessagesRouterInput.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

public protocol IMessagesRouterInput {
    func moduleIsReady()
    func showSettingsView()
}
