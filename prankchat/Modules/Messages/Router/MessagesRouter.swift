//
//  MessagesRouter.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit

public class MessagesRouter {
    
    weak var transitionHandler: ViperModuleTransitionHandler!
    
    func openModule(with transitionHandler: ViperModuleTransitionHandler, transitionStyle: TransitionStyle) {
        let initializer = MessagesInitializer.assemble()
        transitionHandler.openModule(vc: initializer.viewController, style: transitionStyle)
    }
    
    func createModule() -> UIViewController? {
        let initializer = MessagesInitializer.assemble()
        return initializer.viewController
    }
}

extension MessagesRouter: IMessagesRouterInput {
    public func showSettingsView() {
        let initializer = SettingsInitializer.assemble()
//        guard let moduleInput = initializer.viewController as? SettingsViewInput else {
//            fatalError("Could cast to SettingsViewInput")
//        }
        transitionHandler.openModule(vc: initializer.viewController, style: .push)
    }
    
    public func moduleIsReady() {
        // do something
    }
}
