//
//  TFButtonType.swift
//  prankchat
//
//  Created by Victor Barskov on 27/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

enum TFButtonType {
    case voice, iMessage, sms
}


