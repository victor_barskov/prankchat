//
//  ChatTableDataSource.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit
import Reusable

public protocol MessagesTableViewDataSource: UITableViewDataSource, UITableViewDelegate {
    var delegate: MessagesTableViewDelegate? { get set }
    var messages: [Date: [Question]] { get set }
    var sections: [Date] { get set }
}

public protocol MessagesTableViewDelegate: class {
    func updateTable()
}

public class MessagesTableDataSource: NSObject, MessagesTableViewDataSource {
    
    public var messages: [Date: [Question]] = [:]
    public var sections: [Date] = []
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let a = messages[sections[section]] {
            return a.count
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "chatDateView") as? ChatDateView {
            var dateTitle: String
            if sections[section].year == Date().year {
                dateTitle = DateFormatter.dateFormatter.string(from: sections[section])
            } else {
                dateTitle = DateFormatter.dateWithYearFormater.string(from: sections[section])
            }
            if Calendar.current.isDateInToday(sections[section]) {
                let formattedString = NSMutableAttributedString()
                formattedString.bold("Today".localized(with_comment: ""), font: UIFont.boldSystemFont(ofSize: 14))//.normal(" \(sections[section].formatToString("hh:mm", true))")
                dateTitle = "Today".localized(with_comment: "")
                header.setupWith(str: dateTitle, attrStr: formattedString)
                return header
            }
            header.setupWith(str: dateTitle, attrStr: nil)
            return header
        }
        return UIView()
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let array = messages[sections[indexPath.section]] {
            let object = array[indexPath.row]
            if object.isFromUser {
                let cell: RightMessageBubbleCell = tableView.dequeueReusableCell(for: indexPath)
                cell.setupWith(message: object.message, type: object.messageType)
                return cell
            } else {
                let cell: LeftMessageBubbleCell = tableView.dequeueReusableCell(for: indexPath)
                cell.setupWith(message: object.message)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    public weak var delegate: MessagesTableViewDelegate?
}

