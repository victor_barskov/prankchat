//
//  WhatsAppViewInput.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public protocol IMessagesViewInput: class {
    // to place here a code that will notify presenter about view events
    
    func settingsViewShow(show: Bool)
    func dismissKeyboard()
    func setData(messages: [Date: [Question]], sections: [Date])
    func setAvatarObjects(_ name: String?, image: UIImage?, avatarLabel: String?, backColor: UIColor?)
    func addRemoveInputUserTypeView (_ isFromUser: Bool)
    func setAvatarPicture()
    func showHideSettings(_ show: Bool)
    func clearAvatar()
    func back()
}
