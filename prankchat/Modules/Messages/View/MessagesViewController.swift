//
//  MessagesViewController.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit
import Reusable

public class MessagesViewController: UIViewController, ViperModuleTransitionHandler {

    // MARK: - Properties -
    var presenter: MessagesViewOutput!
    lazy var dataSource: MessagesTableViewDataSource = MessagesTableDataSource()
    var imagePicker : UIImagePickerController!
    var isFromUserView: UIView!
    var isFromUser = true
    var settingsViewShowed = false
    var tfButtonType: TFButtonType = .iMessage
    var messageType: MessageType = .iMessage
    
    
    var messages: [Date: [Question]] = [:]
    var sections: [Date] = []
    
    // MARK: - Outlets -
    @IBOutlet weak var restoreButton: UIButton!

    @IBAction func backAction(_ sender: UIButton) {presenter.didTapBack()
}
    @IBAction func infoAction(_ sender: UIButton) {presenter.didTapInfo()}
    @IBAction func photoAction(_ sender: UIButton) {presenter.didTapPhoto()}
    @IBAction func appsAction(_ sender: UIButton) {presenter.didTapApps()}
    
    // Info
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var noMessagesInfoLabel: UILabel!
    @IBOutlet weak var infoView: UIView!
    
    // SettingsView
    @IBOutlet weak var whatsAppBackView: UIView! {
        didSet {
            whatsAppBackView.backgroundColor = .clear
            whatsAppBackView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var settingsView: UIView!
    @IBAction func clearAction(_ sender: UIButton) {presenter.didTapClear()}

    
    // Header
    @IBOutlet weak var avatarView: MessagesAvatarView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var avatarLabel: UILabel!
    @IBAction func avatarButtonAction(_ sender: UIButton) {presenter.didTapChangeAvatar()}
    @IBOutlet weak var nameTextField: UITextField!
    
    // Table
    @IBOutlet weak var table: UITableView! {
        didSet {
            let nibName = UINib(nibName: "ChatDateView", bundle: nil)
            table.register(nibName, forHeaderFooterViewReuseIdentifier: "chatDateView")
            table.backgroundColor = .white
            table.separatorStyle = .none
            table.register(cellType: RightMessageBubbleCell.self)
            table.register(cellType: LeftMessageBubbleCell.self)
            dataSource.delegate = self
            dataSource.messages = [:]
            table.dataSource = dataSource
            table.delegate = dataSource
        }
    }
    
    // InputTextField
    
    @IBOutlet weak var inputTextContentView: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var inputTextContentViewBottomConstraint: NSLayoutConstraint!
    
    @IBAction func textDidChange(_ sender: UITextField) {
        
        guard let charactersCount = sender.text?.count else {return}
        if charactersCount > 0 {
            if isFromUser {
                setButtonInTextField(textFieldButtonType: tfButtonType, textField: sender, selector: #selector(self.sendMessage))
            }
        } else {
            setButtonInTextField(textFieldButtonType: .voice, textField: sender, selector: #selector(self.sendMessage))
        }
    }

    // MARK: - Load -
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.viewLoaded()
        addTapGestureWithCallback()
        setupView()
        addGestureRecognizers ()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        presenter.viewAppeared()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Set up -
    
    public func setAvatarObjects(_ name: String?, image: UIImage?, avatarLabel: String?, backColor: UIColor?) {
        if let name = name {
            nameTextField.text = name
        }
        if let image = image {
            avatarImageView.image = image
        }
        if let avatarLabel = avatarLabel {
            self.avatarLabel.text = avatarLabel
        }
        if let backColor = backColor {
            avatarView.backgroundColor = backColor
        }
    }
    
    func setupView() {
        // Info
        noMessagesInfoLabel.text = "Try to swipe around to feel the functionality".localized(with_comment: "")
        // Input
        inputTextField.layer.cornerRadius = 17.5
        inputTextField.layer.borderWidth = 0.8
        inputTextField.layer.borderColor = UIColor(hex: "C1C1C7", alpha: 0.8).cgColor
        inputTextField.layer.masksToBounds = true
        inputTextField.placeholder = ""
        inputTextField.placeholder = "iMessage".localized(with_comment: "")
        setButtonInTextField(textFieldButtonType: .voice, textField: inputTextField, selector: #selector(self.sendMessage))
        // Header
        avatarView.layer.cornerRadius = avatarView.frame.size.width / 2
        avatarView.clipsToBounds = true
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
        avatarImageView.clipsToBounds = true
        nameTextField.placeholder = "Set name".localized(with_comment: "")
    }
    
    // MARK: - Helpers -

    @objc func keyboardNotification(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve: UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if view.selectedTextField?.tag == 1 {
            } else if view.selectedTextField?.tag == 2 {
                if endFrameY >= UIScreen.main.bounds.size.height {
                    self.inputTextContentViewBottomConstraint?.constant = 0.0
                } else {
                    let window = UIApplication.shared.keyWindow
                    if #available(iOS 11.0, *) {
                        if let bottomPadding = window?.safeAreaInsets.bottom {
                            self.inputTextContentViewBottomConstraint?.constant = (endFrame?.size.height ?? 0.0) - bottomPadding
                        } else {
                            self.inputTextContentViewBottomConstraint?.constant = (endFrame?.size.height ?? 0.0)
                        }
                    } else {
                        // Fallback on earlier versions
                        self.inputTextContentViewBottomConstraint?.constant = (endFrame?.size.height ?? 0.0)
                    }
                }
            }
            if self.settingsViewShowed {
                self.showHideSettings(false)
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: {_ in
                           self.updateTable()
                           self.view.layoutSubviews()
            })
        }
    }
    
    func hideTable(_ hide: Bool) {
        table.isHidden = hide
    }
    
    public func showHideSettings(_ show: Bool) {
        if show {
            if !settingsViewShowed {
                self.settingsViewShowed = true
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.beginFromCurrentState], animations: {
                    self.settingsView.frame = CGRect(x: 0, y: self.header.frame.origin.y + self.header.frame.size.height, width: self.view.frame.size.width, height: self.settingsView.frame.size.height)
                }, completion: {_ in
                })
            }
        } else {
            self.settingsViewShowed = false
            UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.beginFromCurrentState], animations: {
                self.settingsView.frame = CGRect(x: 0, y: self.header.frame.origin.y, width: self.view.frame.size.width, height: self.settingsView.frame.size.height)
            }, completion: {_ in
            })
        }
    }
    
    public func clearAvatar() {
        self.avatarLabel.text = "+"
        self.avatarImageView?.image = nil
        self.avatarLabel.isHidden = false
        self.nameTextField.text = ""
    }
    
    @objc func sendMessage() {
        presenter.didTapSend(message: self.inputTextField.text, isFromUser: isFromUser, messageType: messageType)
        setButtonInTextField(textFieldButtonType: .voice, textField: inputTextField, selector:#selector(self.sendMessage))
        inputTextField.text = ""
    }
    
    @objc func setVoice() {
        presenter.didTapVoice()
    }
    
    func setButtonInTextField(textFieldButtonType: TFButtonType, textField: UITextField, selector: Selector?) {
        
        let button = UIButton(type: .custom)
        let size: CGFloat = textField.frame.height
        
        switch textFieldButtonType {
        case .voice:
            button.setImage(UIImage(named: "voice"), for: .normal)
            guard let selector = selector else {
                break
            }
            button.addTarget(self, action: selector, for: .touchUpInside)
            break
        case .iMessage:
            button.setImage(UIImage(named: "iMessageSend"), for: .normal)
            guard let selector = selector else {
               break
            }
            button.addTarget(self, action: selector, for: .touchUpInside)
            break
        case .sms:
            button.setImage(UIImage(named: "SMSMessageSend"), for: .normal)
            guard let selector = selector else {
                break
            }
            button.addTarget(self, action: selector, for: .touchUpInside)
            break
        }
    
        button.imageEdgeInsets = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        button.frame = CGRect(x: 0, y: 0, width: size, height: size)
        button.layer.cornerRadius = size/2.0
        button.layer.masksToBounds = true
        button.backgroundColor = UIColor.clear
        
        textField.rightView = button
        textField.rightViewMode = .always
    }

    public func back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func setMessagesType(sender: Any?) {
        if let sender = sender as? UIButton {
            // Set message style and send button type style
            switch sender.tag {
            case smsTypeMessageTag:
                self.tfButtonType = .sms
                self.messageType = .sms
                self.inputTextField.placeholder = "Text Message".localized(with_comment: "")
                guard let charactersCount = self.inputTextField.text?.count else {return}
                if charactersCount > 0 {
                    setButtonInTextField(textFieldButtonType: .sms, textField: self.inputTextField, selector: #selector(self.sendMessage))
                } else {
                    setButtonInTextField(textFieldButtonType: .voice, textField: self.inputTextField, selector: #selector(self.setVoice))
                }
                break
            case iMessageTypeTag:
                self.tfButtonType = .iMessage
                self.messageType = .iMessage
                self.inputTextField.placeholder = "iMessage".localized(with_comment: "")
                guard let charactersCount = self.inputTextField.text?.count else {return}
                if charactersCount > 0 {
                    setButtonInTextField(textFieldButtonType: .iMessage, textField: self.inputTextField, selector: #selector(self.sendMessage))
                } else {
                    setButtonInTextField(textFieldButtonType: .voice, textField: self.inputTextField, selector: #selector(self.setVoice))
                }
                break
            default:
                self.tfButtonType = .iMessage
                self.messageType = .iMessage
                setButtonInTextField(textFieldButtonType: .voice, textField: self.inputTextField, selector: #selector(self.setVoice))
                break
            }
        }
    }
}

extension MessagesViewController {
    
    public func addRemoveInputUserTypeView (_ isFromUser: Bool) {
        showHideSettings(false)
        if isFromUser {
            if inputTextField.text?.count ?? 0 > 0 {
                 setButtonInTextField(textFieldButtonType: tfButtonType, textField: inputTextField, selector: #selector(self.sendMessage))
            }
            self.isFromUserView?.removeFromSuperview()
            let isFromUserViewRect = CGRect(x: self.view.frame.size.width / 2, y: header.frame.origin.y + header.frame.size.height, width: self.view.frame.size.width / 2, height: messagesIsFromUserHeightConstantRight)
            isFromUserView = UIView(frame: isFromUserViewRect)
            
            isFromUserView.backgroundColor = .clear
            
            let topPalkaFrame = CGRect(x: 0, y: self.isFromUserView.bounds.origin.y, width: self.isFromUserView.frame.size.width, height: 3.0)
            let topPalka = UIView(frame: topPalkaFrame)
            topPalka.backgroundColor = .deepSkyBlue
            self.isFromUserView.addSubview(topPalka)
            
            let smsButtonWithHeight: CGFloat = 45.0
            let smsButtonRect = CGRect(x: (self.isFromUserView.frame.size.width - smsButtonWithHeight*3)-5, y: topPalka.frame.size.height + 5, width: smsButtonWithHeight, height: smsButtonWithHeight)
            self.isFromUserView.addButton(imageName: "SMSMessageSend", frame: smsButtonRect, color: .clear, name: nil, target: self, tag: smsTypeMessageTag, selector: #selector(self.setMessagesType(sender:)))
            
            let iMButtonWithHeight: CGFloat = 45.0
            let iMButtonRect = CGRect(x: (self.isFromUserView.frame.size.width - smsButtonWithHeight*2), y: topPalka.frame.size.height + 5, width: smsButtonWithHeight, height: iMButtonWithHeight)
            self.isFromUserView.addButton(imageName: "iMessageSend", frame: iMButtonRect, color: .clear, name: nil, target: self, tag: iMessageTypeTag, selector: #selector(self.setMessagesType(sender:)))
            
            UIView.animate(withDuration: 0.2) {
                self.view.addSubview(self.isFromUserView)
            }
            
            delay(3) {
                UIView.animate(withDuration: 0.5, animations: {
                    self.isFromUserView.alpha = 0.0
                }, completion: { (completed) in
                })
            }
            self.isFromUser = true
        } else {
            setButtonInTextField(textFieldButtonType: .voice, textField: self.inputTextField, selector: #selector(self.sendMessage))
            self.isFromUserView?.removeFromSuperview()
            let isFromUserViewRect = CGRect(x: 0, y: header.frame.origin.y + header.frame.size.height, width: self.view.frame.size.width / 2, height: messagesIsFromUserHeightConstantLeft)
            isFromUserView = UIView(frame: isFromUserViewRect)
            isFromUserView.backgroundColor = .lightGray
            UIView.animate(withDuration: 0.2) {
                self.view.addSubview(self.isFromUserView)
            }
            
            delay(2) {
                UIView.animate(withDuration: 0.2, animations: {
                    self.isFromUserView.alpha = 0.0
                }, completion: { (completed) in
                })
            }
            self.isFromUser = false
        }
    }
}

extension MessagesViewController {
    
    func addGestureRecognizers () {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            presenter.didSwipe(gesture.direction)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            presenter.didSwipe(gesture.direction)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            presenter.didSwipe(gesture.direction)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            presenter.didSwipe(gesture.direction)
        }
    }
}

// MARK: Extension Delegates

extension MessagesViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate  {
    
    public func setAvatarPicture() {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {return}
        
        self.dismiss(animated: false, completion: { () -> Void in
            var imageCropVC : RSKImageCropViewController!
            imageCropVC = RSKImageCropViewController(image: image, cropMode: RSKImageCropMode.circle)
            imageCropVC.delegate = self
            self.present(imageCropVC, animated: true, completion: nil)
        })
    }
    
    public func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        self.dismiss(animated: true) {
            self.avatarImageView?.image = croppedImage
            self.avatarLabel.isHidden = true
        }
    }
    
}

extension MessagesViewController: IMessagesViewInput {
    public func setData(messages: [Date : [Question]], sections: [Date]) {
        if messages.count > 0 {
            infoView.isHidden = true
            dataSource.messages = messages
            dataSource.sections = sections
            updateTable()
        } else {
            infoView.isHidden = false
        }
    }
    
    public func settingsViewShow(show: Bool) {
        
    }
}

extension MessagesViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text != nil {
            if view.selectedTextField?.tag == 1 {
                presenter.didTapDoneInKeybordReturn(view.selectedTextField?.text)
            } else if view.selectedTextField?.tag == 2 {
                
                presenter.didTapSend(message: view.selectedTextField?.text, isFromUser: isFromUser, messageType: messageType)
                setButtonInTextField(textFieldButtonType: .voice, textField: inputTextField, selector: #selector(self.sendMessage))
                textField.text = ""
            }
            return false
        }
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if view.selectedTextField?.tag == 1 {
            self.inputTextContentViewBottomConstraint?.constant = 0.0
        }
    }
}

extension MessagesViewController: MessagesTableViewDelegate {
    public func updateTable() {
        table.reloadData()
        table.scrollToBottom(animated: true)
    }
}

extension MessagesViewController {
    func addTapGestureWithCallback() {
        self.view.addTapGestureRecognizer {
            if self.settingsViewShowed {
                self.showHideSettings(false)
            }
            self.dismissKeyboard()
        }
    }
}
