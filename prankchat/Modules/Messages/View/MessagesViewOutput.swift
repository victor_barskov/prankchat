//
//  MessagesViewOutput.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

protocol MessagesViewOutput {
    func viewLoaded()
    func viewAppeared()
    func saveDefaultChatType(_ type: ChatType)
    func didTapShowWhatsApp()
    func didTapDoneInKeybordReturn(_ text: String?)
    func didTapSend(message: String?, isFromUser: Bool, messageType: MessageType)
    func didTapClear()
    func didTapShowSettings()
    func didTapChangeAvatar()
    func didSwipe(_ direction: UISwipeGestureRecognizerDirection)
    func didTapRestore()
    func didTapBack()
    func didTapInfo()
    func didTapPhoto()
    func didTapApps()
    func didTapVoice()
    
    
}
