//
//  RightMessageTableViewCell.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit
import Reusable


class RightMessageBubbleCell: UITableViewCell, NibReusable {

    @IBOutlet weak var messageLabel: WhiteTitleLabel!
    @IBOutlet weak var rightMessageBubbleView: RightMessageBubbleView!
    
    @IBOutlet weak var bubbleImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
    func setupWith(message: String, type: MessageType) {
        messageLabel.text = message
        switch type {
        case .sms:
//            rightMessageBubbleView.backgroundColor = .smsMessageColor
            bubbleImage.image = UIImage(named: "bubble_sent_green")?.resizableImage(withCapInsets:UIEdgeInsetsMake(17, 21, 17, 21),resizingMode: .tile).withRenderingMode(.alwaysOriginal)
            
            break
        case .iMessage:
            bubbleImage.image = UIImage(named: "bubble_sent")?.resizableImage(withCapInsets:UIEdgeInsetsMake(17, 21, 17, 21),resizingMode: .tile).withRenderingMode(.alwaysOriginal)
//            rightMessageBubbleView.backgroundColor = .deepSkyBlue
            break
        default:
            bubbleImage.image = UIImage(named: "bubble_sent")?.resizableImage(withCapInsets:UIEdgeInsetsMake(17, 21, 17, 21),resizingMode: .tile).withRenderingMode(.alwaysOriginal)
//            rightMessageBubbleView.backgroundColor = .deepSkyBlue
            break
        }
        
        
    }

}
