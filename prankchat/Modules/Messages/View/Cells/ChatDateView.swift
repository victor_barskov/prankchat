//
//  ChatDateView.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit

class ChatDateView: UITableViewHeaderFooterView {

    @IBOutlet weak var dateLabel: UILabel!
    
    func setupWith(str: String, attrStr: NSAttributedString?) {
        if let attrStr = attrStr {
            dateLabel.attributedText = attrStr
        } else {
            dateLabel.text = str
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
