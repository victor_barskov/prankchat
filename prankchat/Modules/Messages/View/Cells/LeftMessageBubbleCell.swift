//
//  RightMessageTableViewCell.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit
import Reusable

class LeftMessageBubbleCell: UITableViewCell, NibReusable {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var leftMessageBubbleView: LeftMessageBubbleView!
    @IBOutlet weak var bubbleView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
    func setupWith(message: String) {
        messageLabel.text = message
        bubbleView.image = UIImage(named: "bubble_recieved")?.resizableImage(withCapInsets: UIEdgeInsetsMake(17, 21, 17, 21),resizingMode: .stretch).withRenderingMode(.alwaysOriginal)
    }
    
    

}
