//
//  LeftMessageBubbleView.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit
import Reusable

class LeftMessageBubbleView: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        self.layer.cornerRadius = 16
//        self.clipsToBounds = true
        self.backgroundColor = UIColor.clear
    }
}


