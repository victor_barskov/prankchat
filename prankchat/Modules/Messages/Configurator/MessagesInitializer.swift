//
//  MessagesInitializer.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
import Reusable

public class MessagesInitializer: NSObject, ViperModuleInitializer, ViperInitializer {
    
    @IBOutlet weak public var viewController: MessagesViewController!
    
    public func createModule(values: [Any]) {
        
        let router = MessagesRouter()
        
        let iapService = IAPService()
        let purchaseManager = PurchaseManager(IAPService: iapService)
                
        if viewController == nil {
            viewController = UIStoryboard(name: "Messages", bundle: nil).instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
        }
        
        router.transitionHandler = viewController
        let presenter = MessagesPresenter(view: viewController, router: router, purchaseManager: purchaseManager)
        viewController.presenter = presenter
        
    }
}
