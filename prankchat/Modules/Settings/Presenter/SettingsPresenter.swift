//
//  SettingsPresenter.swift
//  prankchat
//
//  Created by Victor Barskov on 25/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

class SettingsPresenter {
    
    private weak var view: SettingsViewInput!
    private let router: SettingsRouterInput!
    
    init(view: SettingsViewInput, router: SettingsRouterInput) {
        self.view = view
        self.router = router
    }
    
}

extension SettingsPresenter: SettingsViewOutput {
    func viewLoaded() {
        
    }
}
