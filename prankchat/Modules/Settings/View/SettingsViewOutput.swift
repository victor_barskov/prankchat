//
//  SettingsViewOutput.swift
//  prankchat
//
//  Created by Victor Barskov on 25/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
protocol SettingsViewOutput {
    func viewLoaded()
}
