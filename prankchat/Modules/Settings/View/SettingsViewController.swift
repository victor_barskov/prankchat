//
//  SettingsViewController.swift
//  prankchat
//
//  Created by Victor Barskov on 25/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, ViperModuleTransitionHandler {

    var presenter: SettingsViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension SettingsViewController: SettingsViewInput {
    
}
