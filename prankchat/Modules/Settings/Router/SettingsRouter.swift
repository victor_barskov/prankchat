//
//  SettingsRouter.swift
//  prankchat
//
//  Created by Victor Barskov on 25/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public protocol SettingsRouterInput {
    func moduleIsReady()
}

class SettingsRouter {
    weak var transitionHandler: ViperModuleTransitionHandler!
    
    func openModule(with transitionHandler: ViperModuleTransitionHandler, transitionStyle: TransitionStyle) {
        let initializer = SettingsInitializer.assemble()
        transitionHandler.openModule(vc: initializer.viewController, style: transitionStyle)
    }
}

extension SettingsRouter: SettingsRouterInput {
    func moduleIsReady() {
    }
}

