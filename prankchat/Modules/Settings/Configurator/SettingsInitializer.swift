//
//  SettingsInitializer.swift
//  prankchat
//
//  Created by Victor Barskov on 25/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public class SettingsInitializer: NSObject, ViperModuleInitializer, ViperInitializer {
    
    @objc dynamic public var viewController: UIViewController!
    
    public func createModule(values: [Any]) {
        
        let vc = SettingsViewController.loadFromXib()
        let router = SettingsRouter()
        router.transitionHandler = vc
        let presenter = SettingsPresenter(view: vc, router: router)
        vc.presenter = presenter
        self.viewController = vc
        
    }
}
