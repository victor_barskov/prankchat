//
//  IIAPSevice.swift
//  prankchat
//
//  Created by Victor Barskov on 29/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
import StoreKit

protocol IIAPService {
    func requestProductsWithCompletionHandler(_ productIdentifiers: Set<ProductIdentifier>, _ handler: @escaping RequestProductsCompletionHandler)
    func purchaseProduct(_ product: SKProduct)
    func restoreCompletedTransactions()
    func canMakePayments() -> Bool
}
