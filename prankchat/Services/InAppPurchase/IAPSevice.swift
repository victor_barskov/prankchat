//
//  InAppPurchaseSevice.swift
//  prankchat
//
//  Created by Victor Barskov on 29/08/2018.
//  Copyright © 2018 barskov. All rights reserved.

import Foundation
import StoreKit

/// Notification that is generated when a product is purchased.
public let IAPHelperProductPurchasedNotification = "IAPHelperProductPurchasedNotification"
/// Notification that is generated when a product is under purchasing.
public let IAPHelperProductPurchasINGNotification = "IAPHelperProductPurchasINGNotification"
/// Notification that is generated when a product is restored.
public let IAPHelperProductPurchasRestoreNotification = "IAPHelperProductPurchasRestoreNotification"
/// Notification that generated when trnsaction error
public let IAPHelperProductTransactionError = "IAPHelperProductTransactionError"
/// Product identifiers are unique strings registered on the app store.
public typealias ProductIdentifier = String
/// Completion handler called when products are fetched.
public typealias RequestProductsCompletionHandler = (_ success: Bool, _ products: [SKProduct]) -> ()

public class IAPService: NSObject, IIAPService {
    
    fileprivate var purchasedProductIdentifiers = Set<ProductIdentifier>()
    
    // Used by SKProductsRequestDelegate
    fileprivate var productsRequest: SKProductsRequest?
    fileprivate var completionHandler: RequestProductsCompletionHandler?
    
    /// MARK: - User facing API
    
    /// Initialize the helper.  Pass in the set of ProductIdentifiers supported by the app.
    public override init() {
        super.init()
        SKPaymentQueue.default().add(self)
    }
    
    func requestProductsWithCompletionHandler(_ productIdentifiers: Set<ProductIdentifier>, _ handler: @escaping RequestProductsCompletionHandler) {
        completionHandler = handler
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest?.delegate = self
        productsRequest?.start()
    }
    
    func purchaseProduct(_ product: SKProduct) {
        #if DEBUG
        print("Buying \(product.productIdentifier)...")
        #endif
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    func restoreCompletedTransactions() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    
}

// MARK: - SKProductsRequestDelegate

extension IAPService: SKProductsRequestDelegate {
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        #if DEBUG
        print("Loaded list of products...")
        #endif
        let products = response.products
        completionHandler?(true, products)
        clearRequest()
        #if DEBUG
        for p in products {
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
        #endif
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        
        #if DEBUG
        print("Failed to load list of products.")
        print("Error: \(error)")
        #endif
        clearRequest()
    }
    
    fileprivate func clearRequest() {
        productsRequest = nil
        completionHandler = nil
    }
    
    
}

extension IAPService: SKPaymentTransactionObserver {
    /// This is a function called by the payment queue, not to be called directly.
    /// For each transaction act accordingly, save in the purchased cache, issue notifications,
    /// mark the transaction as complete.
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                completeTransaction(transaction)
                break
            case .failed:
                failedTransaction(transaction)
                break
            case .restored:
                restoreTransaction(transaction)
                break
            case .deferred:
                break
            case .purchasing:
                break
            }
        }
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        
    }
    
    public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        let transactionCount = queue.transactions.count
        if transactionCount == 0 {
            systemAlert(title:"Sorry".localized(with_comment: ""), message: "No previous transactions found".localized(with_comment: ""))
        }
    }
    
    fileprivate func completeTransaction(_ transaction: SKPaymentTransaction) {
        #if DEBUG
        print("completeTransaction...")
        #endif
        provideContentForProductIdentifier(transaction.payment.productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    fileprivate func restoreTransaction(_ transaction: SKPaymentTransaction) {
        let productIdentifier = transaction.original!.payment.productIdentifier
        #if DEBUG
        print("restoreTransaction... \(productIdentifier)")
        #endif
        provideContentRestoredIdentifier(productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    // Helper: Saves the fact that the product has been purchased and posts a notification.
    fileprivate func provideContentForProductIdentifier(_ productIdentifier: String) {
        purchasedProductIdentifiers.insert(productIdentifier)
        UserDefaults.standard.set(true, forKey: productIdentifier)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAPHelperProductPurchasedNotification), object: productIdentifier)
    }
    
    fileprivate func provideContentRestoredIdentifier(_ productIdentifier: String) {
        purchasedProductIdentifiers.insert(productIdentifier)
        UserDefaults.standard.set(true, forKey: productIdentifier)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAPHelperProductPurchasRestoreNotification), object: productIdentifier)
    }
    
    fileprivate func failedTransaction(_ transaction: SKPaymentTransaction) {
        #if DEBUG
        print("failedTransaction...")
        #endif
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAPHelperProductTransactionError), object: nil)
        if transaction.error?._code != SKError.paymentCancelled.rawValue {
            #if DEBUG
            print("Transaction error: \(String(describing: transaction.error?.localizedDescription))")
            #endif
            NotificationCenter.default.post(name: Notification.Name(rawValue: IAPHelperProductTransactionError), object: nil)
        }
        SKPaymentQueue.default().finishTransaction(transaction)
    }
}

