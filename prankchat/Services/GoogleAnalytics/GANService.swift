//
//  GANHelper.swift
//

import UIKit

typealias GA = GAN

@objc class GAN: NSObject {
    class var sharedHelper: GAN {
        struct Singleton {
            static let instance = GAN()
        }
        return Singleton.instance
    }
    
    
    var tracker: GAITracker
    
    fileprivate override init() {
        #if DEBUG
        tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-78162499-80")
        #else
        tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-78162499-8")
        #endif
        GAI.sharedInstance().defaultTracker = tracker

    }
    
    class func screen(_ name: String) {
        sharedHelper.tracker.set(kGAIScreenName, value: name)
        let eventTracker: NSObject = GAIDictionaryBuilder.createScreenView().build()
        sharedHelper.tracker.send((eventTracker as! [NSObject : AnyObject]))
    }
    
    class func event(_ name: String) {
        event(name, label: nil)
    }
    
    class func event(_ name: String, label: String?) {
        event(name, label: label, category: "event")
    }
    
    class func event(_ name: String, label: String?, category: String) {
        
        let createEvent = GAIDictionaryBuilder.createEvent(withCategory: category, action: name, label: label, value: 0).build() as [NSObject : AnyObject]
        sharedHelper.tracker.send(createEvent)
    }
    
    class func event(_ name: String, labelTimestamped label: String?) {
        event(name, label: Date().analyticsString + ";" + (label ?? ""))
    }
    
    class func condition(_ condition: Bool, eventS: String, eventF: String, label: String?) {
        event(condition ? eventS : eventF, label: label)
    }
    
    class func condition(_ condition: Bool, eventS: String, eventF: String, labelTimestamped label: String?) {
        event(condition ? eventS : eventF, label: Date().analyticsString + ";" + (label ?? ""))
    }
}

let AnalyticsDateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy/MM/dd"
    formatter.locale = Locale(identifier: "en-US")
    formatter.timeZone = nil
    return formatter
}()

extension Date {
    var analyticsString: String {
        return AnalyticsDateFormatter.string(from: self)
    }
}
