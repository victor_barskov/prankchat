//
//  ApplicationCoordinator.swift
//  prankchat
//
//  Created by Victor Barskov on 26/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
import Firebase
import UserNotifications

class ApplicationCoordinator {
    static let shared = ApplicationCoordinator()
}

extension ApplicationCoordinator: IApplicationCoordinator {
    func start() {
        // Check which type of chat is set by user
        if let window = (UIApplication.shared.delegate as? AppDelegate)?.window {
            window.makeKeyAndVisible()
            if let vc = CatalogueRouter().createModule() {
                window.rootViewController = UINavigationController(rootViewController: vc)
            }
        }
        #if !DEBUG
        FirebaseApp.configure()
        #endif
    }
}
