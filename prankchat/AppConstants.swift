//
//  Constants.swift
//  prankchat
//
//  Created by Victor Barskov on 21/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
import KeychainAccess

public let messagesHeaderHeightConstant: CGFloat = 90.0
public let messagesBottomTextInputViewHeightConstant: CGFloat = 50.0
public let messagesIsFromUserHeightConstantRight: CGFloat = 60.0
public let messagesIsFromUserHeightConstantLeft: CGFloat = 3.0
public let whatsAppIsFromUserHeightConstantRight: CGFloat = 3.0
public let whatsAppIsFromUserHeightConstantLeft: CGFloat = 3.0
public let isFomUserViewHeightConstantLeft: CGFloat = 3.0
public let settingsViewHeightConstant: CGFloat = 60.0
public let setBackgroundViewTag = 111
public let smsTypeMessageTag = 112
public let iMessageTypeTag = 113
public var chatType: ChatType = (UserDefaults.standard.object(forKey: defaultChatType) as? String ?? ChatType.messages.rawValue) == ChatType.messages.rawValue ? .messages: .whatsapp
public let defaultChatType = "defaultChatType"

// Product Identifiers

private let prefix = "com.victorbarskov.yourself."
public let whatsAppModuleAccessIdentifier = prefix + "greenChatModuleAccessIdentifier"


// Key Chain
public let charged = "charged"
public let prankChatKeychain = Keychain(service: "com.victorbarskov.prankchat")

// Events

let whatsAppProductPurchased = "WhatsAppProduct Purchased"
let whatsAppProductRestored = "WhatsAppProduct Restored"

