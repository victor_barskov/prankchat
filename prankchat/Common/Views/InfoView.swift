//
//  InfoView.swift
//  Yourselfchat
//
//  Created by Victor Barskov on 18/09/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit

class InfoView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initContent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initContent()
    }
    
    private  func initContent() {
        
        let view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        addSubview(view)

    }
    
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for:InfoView.self)
        let nib = UINib(nibName: "InfoView", bundle: bundle)
        let view = nib.instantiate(withOwner:self, options: nil)[0] as! UIView
        
        return view
    }

    func show(){
        UIApplication.shared.keyWindow?.addSubview(self)
    }
    func hide(){
        self.removeFromSuperview()
    }
    
}
