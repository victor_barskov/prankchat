//
//  BackgroundWhenLongPressForSetView.swift
//  prankchat
//
//  Created by Victor Barskov on 28/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import UIKit

class BackgroundWhenLongPressForSetView: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = self.frame.size.height / 4
        self.clipsToBounds = true
        self.backgroundColor = .black
        self.alpha = 0.6
//        self.addShadow(with: UIColor.coolGray.cgColor)
    }
}
