//
//  RadiaGradientLayer.swift
//  prankchat
//
//  Created by Victor Barskov on 21/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

class RadialGradientLayer: CALayer {
    var isCircle: Bool = false
    
    var width: CGFloat?
    
    var center: CGPoint {
        return CGPoint(x: bounds.width/2, y: bounds.height/2)
    }
    
    var radius: CGFloat {
        if isCircle {
            return min(bounds.width, bounds.height) / 1.7
        } else {
            return (bounds.width + bounds.height) / 2
        }
    }
    
    private var startRadius: CGFloat {
        let width = self.width ?? radius
        return radius - width
    }
    
    var colors: [UIColor] = [UIColor.clear, UIColor.clear] {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var cgColors: [CGColor] {
        return colors.map({ (color) -> CGColor in
            return color.cgColor
        })
    }
    
    override init(layer: Any) {
        super.init()
        needsDisplayOnBoundsChange = true
    }
    
    override init() {
        super.init()
        needsDisplayOnBoundsChange = true
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
    }
    
    override func draw(in ctx: CGContext) {
        ctx.saveGState()
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        guard let gradient = CGGradient(colorsSpace: colorSpace,
                                        colors: cgColors as CFArray,
                                        locations: nil) else {
                                            return
        }
        ctx.drawRadialGradient(gradient,
                               startCenter: center,
                               startRadius: startRadius,
                               endCenter: center,
                               endRadius: radius,
                               options: CGGradientDrawingOptions(rawValue: 0))
    }
}
