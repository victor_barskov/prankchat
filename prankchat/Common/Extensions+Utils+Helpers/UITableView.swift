//
//  UITableView.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

extension UITableView {
    func scrollToBottom(animated: Bool) {
        
        let sections = numberOfSections
        
        if sections > 0 {
            let rows = numberOfRows(inSection: sections - 1)
            let last = IndexPath(row: rows - 1, section: sections - 1)
            DispatchQueue.main.async {
                self.scrollToRow(at: last, at: .bottom, animated: false)
            }
        }
    }
}
