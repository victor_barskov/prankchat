//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation

public extension String {
    func localized (with_comment comment: String) -> String {
        return NSLocalizedString(self, comment: comment)
    }
}

public extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.characters.count
    }
}

public extension String {
    
    enum DeviceLocale: String {
        
        //    case Unknown = "en"
        //    case RU = "ru_RU"
        //    case US = "en_US"
        //    case BR = "pt_BR"
        
        case Unknown = "en"
        case RU = "RU"
        case US = "US"
        case BR = "BR"
        
    }
    
    var deviceLoc: DeviceLocale {
        let locale = Locale.current.identifier
        guard let loc = DeviceLocale(rawValue: locale) else { return .Unknown }
        return loc
    }
    
    var deviceReg:  DeviceLocale {
        guard let locale = Locale.current.regionCode else { return .Unknown}
        guard let loc = DeviceLocale(rawValue: locale) else { return .Unknown }
        return loc
    }
    
}

public extension String {
    func stringByRemovingAll(characters: [Character]) -> String {
        return String(self.characters.filter({ !characters.contains($0) }))
    }
}

// Extension to remove characters from phone number

public extension String {
    
    func replace(_ string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(" ", replacement: "")
    }
    func removeDash () -> String {
        return self.replace("-", replacement: "")
    }
    func removeBracketRight () -> String {
        return self.replace(")", replacement: "")
        
    }
    func removeBracketLeft () -> String {
        return self.replace("(", replacement: "")
    }
    
    func removeLeftWard () -> String {
        return self.replace("<", replacement: "")
    }
    
    func removeRightWard () -> String {
        return self.replace(">", replacement: "")
    }
    
}

extension String {
    var isEmptyField: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String, font: UIFont) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: font]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        return self
    }
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        return self
    }
}
