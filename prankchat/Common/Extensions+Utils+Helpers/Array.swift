//
//  Array.swift
//  emop
//
//  Created by Victor Barskov on 21/07/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import Foundation

// Insert at the beginning of the array without loosing the item at the first position
extension Array{
    mutating func appendAtBeginning(newItem : Element) {
        let copy = self
        self = []
        self.append(newItem)
        self.append(contentsOf: copy)
    }
}


// Append unique object
//extension Array where Element: Hashable {
//    mutating func appendUniqueObject(object: Iterator.Element) {
//        if contains(object) == false {
//            append(object)
//        }
//    }
//}

// Append unique object
extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() // the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() // keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        return arrayOrdered
    }
}
