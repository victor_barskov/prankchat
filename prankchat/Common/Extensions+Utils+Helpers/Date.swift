//
//  Date.swift
//  emop
//
//  Created by Victor Barskov on 19/07/2017.
//  Copyright © 2017 wifiasyougo. All rights reserved.
//

import Foundation

// MARK: NSDATE EXTENSION

public extension Date
    
    // link to source http://stackoverflow.com/questions/26198526/nsdate-comparison-using-swift
    
{
    
    func formatToString(_ format: String,_ localTimezone:Bool = false) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Calendar.current.locale
        if (!localTimezone)
        {
            formatter.timeZone = TimeZone(abbreviation: "UTC")
        }
        
        return formatter.string(from: self)
    }
    
    func isGreaterThanDate(_ dateToCompare : Date) -> Bool
        
    {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending
        {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    
    func isLessThanDate(_ dateToCompare : Date) -> Bool
    {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending
        {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    
    func isEqualWithDate(_ dateToCompare : Date) -> Bool
    {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame
        {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    
    func addDays(_ daysToAdd : Int) -> Date
        
    {
        let secondsInDays : TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded : Date = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    
    func addHours(_ hoursToAdd : Int) -> Date
        
    {
        let secondsInHours : TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded : Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    func addMinutes(_ minutesToAdd : Int) -> Date
        
    {
        let secondsInHours : TimeInterval = Double(minutesToAdd) * 60
        let dateWithHoursAdded : Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    func addMonth(_ monthsToAdd : Int) -> Date
        
    {
        let dateComponents = DateComponents(month: monthsToAdd)
        return Calendar.current.date(byAdding: dateComponents, to: self)!
    }
    
}

public extension Date {
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        //        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
        let dateComponents = DateComponents(month: 1)
        return Calendar.current.date(byAdding: dateComponents, to: self.startOfMonth())!
    }
    
    // print(Date().startOfMonth()) // "2016-09-01 07:00:00 +0000\n"
    // print(Date().endOfMonth()) // "2016-09-30 07:00:00 +0000\n"
}

public extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date? {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)
    }
}

public extension Date {
    
    func getDaysInMonth() -> Int {
        let calendar = Calendar.current
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        return numDays
    }
    
}

func generateDatesArrayBetweenTwoDates(startDate: Date? , endDate:Date?) ->[Date]
{
    var datesArray: [Date] =  [Date]()
    var startDate = startDate
    let calendar = Calendar.current
    
    let fmt = DateFormatter()
    fmt.dateFormat = "yyyy-MM-dd"
    if startDate != nil && endDate != nil {
        while startDate! <= endDate! {
            datesArray.append(startDate!)
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate!)!
        }
    }
    
    return datesArray
}

extension TimeZone {
    public static var gmt: TimeZone { return TimeZone(secondsFromGMT: 0)! }
}


extension Date
{
    // MARK: - Convenience
    
    public static func from(_ date: String, format: String, locale: Locale = .current, timeZone: TimeZone = .current) -> Date?
    {
        let formatter = DateFormatter()
        
        formatter.dateFormat = format
        formatter.locale = locale
        formatter.timeZone = timeZone
        
        return formatter.date(from: date)
    }
    
    public func format(_ format: String, locale: Locale = .current, timeZone: TimeZone = .current) -> String
    {
        let formatter = DateFormatter()
        
        formatter.dateFormat = format
        formatter.locale = locale
        formatter.timeZone = timeZone
        
        return formatter.string(from: self)
    }
    
    public func date(byAdding value: Int, component: Calendar.Component, calendar: Calendar = .current) -> Date?
    {
        var components = DateComponents()
        
        components.setValue(value, for: component)
        
        return calendar.date(byAdding: components, to: self)
    }
    
    public func date(bySetting component: Calendar.Component, value: Int, calendar: Calendar = .current) -> Date? {
        return calendar.date(bySetting: component, value: value, of: self)
    }
    
    public func component(_ component: Calendar.Component, calendar: Calendar = .current) -> Int {
        return calendar.component(component, from: self)
    }
    
    // MARK: - String representation
    
    /// Easier to read string representation of date
    public var relativeValue: String {
        let formatter = DateFormatter()
        
        formatter.timeStyle = .short
        formatter.dateStyle = .short
        formatter.doesRelativeDateFormatting = true
        
        return formatter.string(from: self)
    }
}




