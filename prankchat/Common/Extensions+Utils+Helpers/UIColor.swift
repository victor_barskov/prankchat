//
//  UIColor.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/5/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIColor {
    static func getRandomColor() -> UIColor {
        return UIColor(red:   .getRandomFloat(),
                       green: .getRandomFloat(),
                       blue:  .getRandomFloat(),
                       alpha: 1.0)
    }
}

public extension UIColor {

    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }

    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
}

public extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: NSCharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

public extension UIColor {
    class func brandColor(_ alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: 255.0/255.0, green: 22.0/255.0, blue: 0.0/255.0, alpha: alpha)
    }
    class func spaceGrayWithAlpha(_ alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: 165.0/255.0, green: 165.0/255.0, blue: 165.0/255.0, alpha: alpha)
    }
    class func placeHolderSpaceGrey(_ alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.4)
    }
    class func clusterPinColor() -> UIColor {
        return UIColor(red: 0.0/255.0, green: 84.0/255.0, blue: 166.0/255.0, alpha: 1)
    }
    class func doit() -> UIColor {
        return UIColor(red: 229.0/255.0, green: 229.0/255.0, blue: 234.0/255.0, alpha: 0.8)
    }
    class func frank() -> UIColor {
        return UIColor(red: 20.0/255.0, green: 135.0/255.0, blue: 254.0/255.0, alpha: 0.895)
    }
    
    class func darkBlueBrand(_ alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: 38.0/255.0, green: 38.0/255.0, blue: 103.0/255.0, alpha: alpha)
    }
    class func tiffanyBlue(alpha: CGFloat=1.0) -> UIColor {
        return UIColor(red: 0.0/255.0, green: 214.0/255.0, blue: 190.0/255.0, alpha: alpha)
    }
    
    class func grey231() -> UIColor {
        return UIColor(red: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
    }
    
    class func yellowEMOP() -> UIColor {
        return UIColor(red: 244.0/255.0, green: 216.0/255.0, blue: 73.0/255.0, alpha: 1.0)
    }
    
    class func whiteWithAlpha(_ alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: alpha)
    }
    
}

extension UIColor {
    class func colorWithHex(hex: Int) -> UIColor {
        let red = CGFloat((hex & 0xff0000) >> 16)
        let green = CGFloat((hex & 0x00ff00) >> 8)
        let blue = CGFloat(hex & 0x0000ff)
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
}

public extension UIColor {
    public convenience init(_ red: Int, _ green: Int, _ blue: Int, alpha: CGFloat) {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
    
    public convenience init(_ red: Int, _ green: Int, _ blue: Int) {
        self.init(red, green, blue, alpha: 1.0)
    }
}

extension UIColor {
    
    static let main: UIColor = UIColor(21, 126, 251)
    static let navbarBackground: UIColor = UIColor.black
    static let navbarText: UIColor = .white
    static let navbarTint: UIColor = UIColor(21, 126, 251)
    static let pushBackground: UIColor = UIColor(6, 7, 12)
    static let separator: UIColor = UIColor(201, 207, 212) //UIColor(223, 227, 230)
    static let border: UIColor = UIColor(201, 207, 212) //UIColor(223, 227, 230)
    
    static let gray: UIColor = UIColor(133, 138, 143)
    static let coolGray: UIColor = UIColor(156, 161, 165)
    static let green: UIColor = UIColor(122, 195, 42)
    
    static let dimmingBackground: UIColor = UIColor(22, 25, 32, alpha: 0.86)
    static let iceBlue: UIColor = UIColor(246, 250, 255)
    static let deepSkyBlue: UIColor = UIColor(21, 126, 251)
    static let lightBlue: UIColor = UIColor(239, 246, 255)
    
    static let whatsAppRightBubbleBackgroundColor: UIColor = UIColor(220, 248, 198)
    static let whatsAppLeftBubbleBackgroundColor: UIColor = UIColor(250, 250, 250, alpha: 1.0)
    static let whatsAppBrandColor = UIColor(69,196,84)
    static let smsMessageColor = UIColor(29,202,60)
    static let appBrandColor = UIColor(253,17,0)
    
    static let text: UIColor = UIColor.black
    static let placeholderColor: UIColor = UIColor.white.withAlphaComponent(0.2)
    static let subtext: UIColor = UIColor(220, 220, 220)
    static let background: UIColor = UIColor.black
    static let contentBackground: UIColor = UIColor.black
    static let arrowBackground: UIColor = UIColor(244, 245, 246)
    static let itemBackground: UIColor = UIColor.black
    static let underscore: UIColor = UIColor(0, 139, 125)
    
    static let sideMenuItem = UIColor.clear
    static let sideMenuSelection = UIColor.white
    static let sideMenuTitleSelection = UIColor.black
    
    static let buttonActive = UIColor(21, 126, 251)
    static let buttonInactive = UIColor(223, 227, 230)
    static let buttonRed = UIColor(240, 54, 54)
    
    static let authProceedButton = UIColor(0, 139, 125)
    static let authConfirmButton = UIColor(7, 99, 95)
    
    static let placeholderViewButton = UIColor(21, 126, 251)
    static let placeholderViewText = UIColor.white
    
    static var partnerHeaderBackground = UIColor.black
    
    static let routeIcon: UIColor = UIColor.white
    static let mapRoute = UIColor(22, 25, 32)
    static let mapTint = UIColor(22, 25, 32)
    static let mapActionsBackground = UIColor.white
    
    static var indicatorCircleBackColor = UIColor.clear
    static var indicatorSemiArcBackColor = UIColor(223, 227, 230)
    static var indicatorNeutralColor: UIColor { return UIColor.deepSkyBlue }
    static var indicatorGoodColor: UIColor { return UIColor.green }
    static var indicatorMediumColor = UIColor(247, 192, 24)
    static var indicatorBadColor = UIColor(240, 54, 54)
    
    static var timePlotLineColor: UIColor { return UIColor.indicatorSemiArcBackColor }
    
    static let error: UIColor = UIColor(183, 22, 26)
    
    static let paleGreyTwo: UIColor = UIColor(234, 236, 241)
}
