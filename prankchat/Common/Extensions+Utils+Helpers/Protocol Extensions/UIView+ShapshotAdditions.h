//
// Created by Sergey Pronin on 8/23/13.
// Copyright (c) 2013 Empatika. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


@import UIKit;

@interface UIView (ShapshotAdditions)

- (UIImage *)snapshotImage;

@end