//
//  Handlers.swift
//  WifiCaptive
//
//  Created by Victor Barskov on 01/11/2017.
//  Copyright © 2017 plotkin. All rights reserved.
//

import Foundation
import UIKit

protocol ErrorHandling {
    func handleError(error: String)
}

protocol SuccessHandling {
    func handleSuccess(alert: String)
    func  handleSuccessWithAction(alert: String, completion: @escaping () -> ())
}

extension ErrorHandling where Self: UIViewController {
    
    func handleError(error: String) {
        let alert = UIAlertController(title: "Error".localized(with_comment: ""), message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss".localized(with_comment: ""), style: .cancel, handler: nil))
        present(alert, animated: true, completion: {
            alert.view.shake()
        })
    }
    
    func handleErrorAndDismiss(error: String) {
        let alert = UIAlertController(title: "Error".localized(with_comment: ""), message: error, preferredStyle: .alert)
        let action = UIAlertAction(title:"Ok".localized(with_comment: ""), style: .cancel) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        present(alert, animated: true, completion: {
            alert.view.shake()
        })
    }
}

extension SuccessHandling where Self: UIViewController {
    
    func handleSuccess(alert: String) {
        let alert = UIAlertController(title: "Success".localized(with_comment: ""), message: alert, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok".localized(with_comment: ""), style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func handleSuccessWithAction(alert: String, completion: @escaping () -> ()) {
        let alert = UIAlertController(title: "Success".localized(with_comment: ""), message: alert, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok".localized(with_comment: ""), style: .cancel, handler: {[unowned self]
            alert -> Void in
                    completion()
        }))
        present(alert, animated: true, completion: nil)
    }
    
}
