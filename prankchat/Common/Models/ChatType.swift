//
//  ChatType.swift
//  prankchat
//
//  Created by Victor Barskov on 26/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public enum ChatType: String {
    case messages, whatsapp
}
