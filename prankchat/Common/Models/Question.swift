//
//  Question.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public struct Question {
    var message: String
    var time: String
    var isFromUser: Bool
    var messageType: MessageType
}
