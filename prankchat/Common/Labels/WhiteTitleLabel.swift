//
//  WhiteTitleLabel.swift
//  prankchat
//
//  Created by Victor Barskov on 22/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

class WhiteTitleLabel: UILabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        self.textColor = UIColor.white
    }
}
