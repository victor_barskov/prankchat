//
//  PurchaseManager.swift
//  prankchat
//
//  Created by Victor Barskov on 29/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
import StoreKit

class PurchaseManager: IPurchaseManager {
    
    var IAPService: IIAPService
    
    init(IAPService: IIAPService) {
        self.IAPService = IAPService
    }
    
    func purchaseProduct(_ product: SKProduct) {
        IAPService.purchaseProduct(product)
    }
    
    func requestProducts(_ idendifiers: Set<ProductIdentifier>, cometion: @escaping ([SKProduct]?) -> ()) {
        IAPService.requestProductsWithCompletionHandler(idendifiers) {(success, products) in
            if success {
                cometion(products)
            } else {
                cometion(nil)
            }
        }
    }
   
    func restore() {
        IAPService.restoreCompletedTransactions()
    }
    
    func userCanMakePayments() -> Bool {
        return IAPService.canMakePayments()
    }

}



