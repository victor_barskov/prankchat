//
//  IPurchaseManager.swift
//  prankchat
//
//  Created by Victor Barskov on 29/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation
import StoreKit

protocol IPurchaseManager {
    func purchaseProduct(_ product: SKProduct)
    func requestProducts(_ idendifiers: Set<ProductIdentifier>, cometion: @escaping (_ products: [SKProduct]?) -> ())
    func restore()
    func userCanMakePayments() -> Bool
}
