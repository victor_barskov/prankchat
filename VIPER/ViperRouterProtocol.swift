//
//  ViperRouterProtocol.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

import Foundation

public protocol ViperRouterProtocol {
    var transitionHandler: ViperModuleTransitionHandler! { get set }
}

