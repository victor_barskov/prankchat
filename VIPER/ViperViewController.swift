//
//  ViperViewController.swift
//  prankchat
//
//  Created by Victor Barskov on 24/08/2018.
//  Copyright © 2018 barskov. All rights reserved.
//

/// IMPORTANT:
/// THIS .swift FILE SHOULD NOT BE CHANGED OR UPDATED!
/// ANY MODIFICATIONS CAN LEAD TO LOSS OF INTEGRITY!

import UIKit

public protocol ModuleViewWithOutput {
    var presenter: ViperModuleInputProtocol { get }
}

/// !!! THIS PROTOCOL MUST NOT BE CHANGED OR UPDATED !!!
/// !!! DO NOT ADD ANY ADDITIONAL CODE OR EDIT EXISTING !!!

public protocol ViperViewControllerProtocol: ModuleViewWithOutput {
    associatedtype OutputType
    var output: OutputType! { get set }
}

/// !!! THESE CLASSES MUST NOT BE CHANGED OR UPDATED !!!
/// !!! DO NOT ADD ANY ADDITIONAL CODE OR EDIT EXISTING !!!

open class ViperViewController: UIViewController {
    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handleSegue(segue, sender: sender)
    }
}

open class ViperTableViewController: UITableViewController {
    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handleSegue(segue, sender: sender)
    }
}

open class ViperCollectionViewController: UICollectionViewController {
    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.handleSegue(segue, sender: sender)
    }
}

class ViperNavigationController: UINavigationController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handleSegue(segue, sender: sender)
    }
}

/// !!! THIS EXTENSION MUST NOT BE CHANGED OR UPDATED !!!
/// !!! DO NOT ADD ANY ADDITIONAL CODE OR EDIT EXISTING !!!

public extension ViperViewControllerProtocol where Self: UIViewController {
    var presenter: ViperModuleInputProtocol {
        guard let output = output as? ViperModuleInputProtocol else {
            fatalError("Presenter should conform 'ViperModuleInputProtocol'")
        }
        
        return output
    }
}

extension ViperViewController: ViperModuleTransitionHandler {}
extension ViperNavigationController: ViperModuleTransitionHandler {}
extension ViperTableViewController: ViperModuleTransitionHandler {}
extension ViperCollectionViewController: ViperModuleTransitionHandler {}
